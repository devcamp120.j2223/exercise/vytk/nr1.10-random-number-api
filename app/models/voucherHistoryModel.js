const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const voucherHistorySchema = new Schema({
  _id: {
    type: mongoose.Types.ObjectId,
    required:true
  },
  user: {
    type: mongoose.Types.ObjectId,
    ref:"user",
    required:true
  },
  voucher: {
    type: mongoose.Types.ObjectId,
    ref:"voucher",
    required:true
  },
  createdAt: {
    type: Date,
    default: Date.now()
  },
  updatedAt: {
    type: Date,
    default: Date.now()
  }
});

module.exports = mongoose.model("voucherHistory", voucherHistorySchema);