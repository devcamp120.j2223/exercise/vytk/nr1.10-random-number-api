const express = require('express');
const router = express.Router();

//Import prizeHistoryController vào
const {
  createVoucherHistory,
  getAllVoucherHistory,
  getVoucherHistoryById,
  updateVoucherHistoryById,
  deleteVoucherHistoryById
} = require('../controllers/voucherHistoryController');


router.post('/voucher-histories', createVoucherHistory);

router.get('/voucher-histories', getAllVoucherHistory);

router.get('/voucher-histories/:historyId', getVoucherHistoryById);

router.put('/voucher-histories/:historyId', updateVoucherHistoryById);

router.delete('/voucher-histories/:historyId', deleteVoucherHistoryById);


module.exports = router;
