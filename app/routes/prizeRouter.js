const express = require('express');
const router = express.Router();

//Import các hàm trong prizeController
const {
  createPrize,
  getAllPrize,
  getPrizeById,
  updatePrizeById,
  deletePrizeById
} = require('../controllers/prizeController');


router.post('/prizes', createPrize);

router.get('/prizes', getAllPrize);

router.get('/prizes/:prizeId', getPrizeById);

router.put('/prizes/:prizeId', updatePrizeById);

router.delete('/prizes/:prizeId', deletePrizeById);

module.exports = router;
