const express = require('express');
const router = express.Router();

//Import các hàm trong userController
const {
  createUser,
  getAllUser,
  getUserById,
  updateUserById,
  deleteUserById
} = require('../controllers/userController');


router.post('/users', createUser);

router.get('/users', getAllUser);

router.get('/users/:userId', getUserById);

router.put('/users/:userId', updateUserById);

router.delete('/users/:userId', deleteUserById);

module.exports = router;
