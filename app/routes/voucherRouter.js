const express = require('express');
const router = express.Router();

//Import các hàm trong voucherController
const {
  createVoucher,
  getAllVoucher,
  getVoucherById,
  updateVoucherById,
  deleteVoucherById
} = require('../controllers/voucherController');


router.post('/vouchers', createVoucher);

router.get('/vouchers', getAllVoucher);

router.get('/vouchers/:voucherId', getVoucherById);

router.put('/vouchers/:voucherId', updateVoucherById);

router.delete('/vouchers/:voucherId', deleteVoucherById);

module.exports = router;
