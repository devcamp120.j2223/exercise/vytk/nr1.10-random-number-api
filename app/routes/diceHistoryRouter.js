const express = require('express');
const router = express.Router();

//Import orderController vào
const {
  createDiceHistory,
  getAllDiceHistory,
  getDiceHistoryById,
  updateDiceHistoryById,
  deleteDiceHistoryById
} = require('../controllers/diceController');


router.post('/dice-histories', createDiceHistory);

router.get('/dice-histories', getAllDiceHistory);

router.get('/dice-histories/:diceHistoryId', getDiceHistoryById);

router.put('/dice-histories/:diceHistoryId', updateDiceHistoryById);

router.delete('/dice-histories/:diceHistoryId', deleteDiceHistoryById);


module.exports = router;
