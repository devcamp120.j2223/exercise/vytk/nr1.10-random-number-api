const express = require('express');
const router = express.Router();

//Import prizeHistoryController vào
const {
  createPrizeHistory,
  getAllPrizeHistory,
  getPrizeHistoryById,
  updatePrizeHistoryById,
  deletePrizeHistoryById
} = require('../controllers/prizeHistoryController');


router.post('/prize-histories', createPrizeHistory);

router.get('/prize-histories', getAllPrizeHistory);

router.get('/prize-histories/:historyId', getPrizeHistoryById);

router.put('/prize-histories/:historyId', updatePrizeHistoryById);

router.delete('/prize-histories/:historyId', deletePrizeHistoryById);


module.exports = router;
