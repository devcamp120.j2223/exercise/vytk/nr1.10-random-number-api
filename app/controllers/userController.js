
//Import userModel vào
const userModel = require('../models/userModel');

//Khai báo thư viện mongoose để tạo _id
var mongoose = require('mongoose');

//Hàm tạo mới user
const createUser = (request, response) => {
  // B1: Thu thập dữ liệu
  let bodyRequest = request.body;
  let historyId = request.params.diceId;
  // B2: Kiểm tra dữ liệu
  if (!bodyRequest.userName) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "userName is required"
    })
  }
  if (!bodyRequest.firstName) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "firstName is required"
    })
  }
  if (!bodyRequest.lastName) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "lastName is required"
    })
  }
  // B3: Thao tác với cơ sở dữ liệu
  let createUser = {
    _id: new mongoose.Types.ObjectId(),
    userName: bodyRequest.userName,
    firstName: bodyRequest.firstName,
    lastName: bodyRequest.lastName
  }
  userModel.create(createUser, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      // diceHistoryModel.findByIdAndUpdate(historyId,
      //   {
      //     $push: { user: data._id }
      //   },
      //   (err, data) => {
      //     if (err) {
      //       return response.status(500).json({
      //         status: "Error 500: Internal server error",
      //         message: err.message
      //       })
      //     } else {
      //       return response.status(201).json({
      //         status: "Create User Success",
      //         data: data
      //       })
      //     }
      //   }
      // )
      response.status(200).json({
        status: "Success: Create User success",
        data: data
      })
    }

  })
}
//Hàm lấy hết danh sách users
const getAllUser = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  //B2: Validate dữ liệu
  //B3: Thao tác với cơ sở dữ liệu
  userModel.find((error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Get all users success",
        data: data
      })
    }
  })
}
//Hàm lấy ra 1 user theo ID
const getUserById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let userId = request.params.userId;
  console.log(userId);
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "User ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  userModel.findById(userId, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Get user by id success",
        data: data
      })
    }
  })
}
//Hàm cập nhật 1 user theo ID
const updateUserById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let userId = request.params.userId;
  let bodyRequest = request.body;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "User ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  let updateUser = {
    userName: bodyRequest.userName,
    firstName: bodyRequest.firstName,
    lastName: bodyRequest.lastName
  }
  userModel.findByIdAndUpdate(userId, updateUser, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Update user by id success",
        data: data
      })
    }
  })
}

//Hàm xoá 1 user theo ID
const deleteUserById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  //let historyId = request.params.diceId;
  let userId = request.params.userId;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "User ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  userModel.findByIdAndDelete(userId, (error) => {
    if(error) {
        return response.status(500).json({
            status: "Error 500: Internal server error",
            message: error.message
        })
    } else {
        // diceHistoryModel.findByIdAndUpdate(historyId, 
        //     {
        //         $pull: { user: userId } 
        //     },
        //     (err, updatedCourse) => {
        //         if(err) {
        //             return response.status(500).json({
        //                 status: "Error 500: Internal server error",
        //                 message: err.message
        //             })
        //         } else {
        //             return response.status(204).json({
        //                 status: "Success: Delete user success"
        //             })
        //         }
        //     })
        response.status(204).json({
          status: "Success: Delete User success"
        })
    }
})
}
module.exports = {
  createUser,
  getAllUser,
  getUserById,
  updateUserById,
  deleteUserById
};