//Import voucherHistoryModel vào
const voucherHistoryModel = require('../models/voucherHistoryModel');

//Khai báo thư viện mongoose để tạo _id
var mongoose = require('mongoose');

//Hàm tạo mới 1 orders
const createVoucherHistory = (request, response) => {
  // B1: Thu thập dữ liệu
  let bodyRequest = request.body;
  // B2: Kiểm tra dữ liệu
  if (!bodyRequest.user) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "user is required"
    })
  }
  if (!bodyRequest.voucher) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "voucher is required"
    })
  }
  // B3: Thao tác với cơ sở dữ liệu
  let createVoucherHistory = {
    _id: new mongoose.Types.ObjectId(),
    user:mongoose.Types.ObjectId(),
    voucher: bodyRequest.voucher
  }
  voucherHistoryModel.create(createVoucherHistory, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      
      response.status(200).json({
        status: "Success: Update history by id success",
        data: data
      })
    }

  })
}

//Hàm lấy hết voucher history
const getAllVoucherHistory = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  //B2: Validate dữ liệu
  //B3: Thao tác với cơ sở dữ liệu
  voucherHistoryModel.find((error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Get all voucher history success",
        data: data
      })
    }
  })
}

//Hàm lấy 1 voucher history theo ID
const getVoucherHistoryById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let historyId = request.params.historyId;
  console.log(historyId);
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(historyId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "History ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  voucherHistoryModel.findById(historyId, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Get voucher history by id success",
        data: data
      })
    }
  })
}

//Hàm update voucher history by ID
const updateVoucherHistoryById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let historyId = request.params.historyId;
  let bodyRequest = request.body;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(historyId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "Voucher history ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  let updateHistory = {
    user:mongoose.Types.ObjectId(),
    voucher: bodyRequest.voucher
  }
  voucherHistoryModel.findByIdAndUpdate(historyId, updateHistory, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Update voucher history by id success",
        data: data
      })
    }
  })
}

//Hàm xoá 1 voucher history by ID
const deleteVoucherHistoryById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let historyId = request.params.historyId;
  //let userId = request.params.userId;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(historyId)) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "History ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  voucherHistoryModel.findByIdAndDelete(historyId, (error) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(204).json({
        status: "Success: Delete voucher history success"
      })
    }
  })

}
module.exports = {
    createVoucherHistory: createVoucherHistory,
    getAllVoucherHistory: getAllVoucherHistory,
    getVoucherHistoryById: getVoucherHistoryById,
    updateVoucherHistoryById: updateVoucherHistoryById,
    deleteVoucherHistoryById: deleteVoucherHistoryById
};