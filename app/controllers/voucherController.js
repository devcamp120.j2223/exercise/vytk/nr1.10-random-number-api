
//Import voucherModel vào
const voucherModel = require('../models/voucherModel');

//Khai báo thư viện mongoose để tạo _id
var mongoose = require('mongoose');

//Hàm tạo mới user
const createVoucher = (request, response) => {
  // B1: Thu thập dữ liệu
  let bodyRequest = request.body;
  // B2: Kiểm tra dữ liệu
  if (!bodyRequest.code) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "code is required"
    })
  }
  if (!bodyRequest.discount) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "discount is required"
    })
  }
  // B3: Thao tác với cơ sở dữ liệu
  let createVoucher = {
    _id: new mongoose.Types.ObjectId(),
    code: bodyRequest.code,
    discount: bodyRequest.discount,
    note: bodyRequest.note
  }
  voucherModel.create(createVoucher, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Create voucher success",
        data: data
      })
    }

  })
}
//Hàm lấy hết danh sách voucher
const getAllVoucher = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  //B2: Validate dữ liệu
  //B3: Thao tác với cơ sở dữ liệu
  voucherModel.find((error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Get all vouchers success",
        data: data
      })
    }
  })
}
//Hàm lấy ra 1 voucher theo ID
const getVoucherById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let VoucherId = request.params.voucherId;
  console.log(VoucherId);
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(VoucherId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "Voucher ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  voucherModel.findById(VoucherId, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Get voucher by id success",
        data: data
      })
    }
  })
}
//Hàm cập nhật 1 voucher theo ID
const updateVoucherById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let voucherId = request.params.voucherId;
  let bodyRequest = request.body;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(voucherId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "User ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  let updateVoucher = {
    code: bodyRequest.code,
    discount: bodyRequest.discount,
    note: bodyRequest.note
  }
  voucherModel.findByIdAndUpdate(voucherId, updateVoucher, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Update voucher by id success",
        data: data
      })
    }
  })
}

//Hàm xoá 1 voucher theo ID
const deleteVoucherById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let voucherId = request.params.voucherId;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(voucherId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "Voucher ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  voucherModel.findByIdAndDelete(voucherId, (error) => {
    if(error) {
        return response.status(500).json({
            status: "Error 500: Internal server error",
            message: error.message
        })
    } else {
        response.status(204).json({
          status: "Success: Delete voucher success"
        })
    }
})
}
module.exports = {
    createVoucher: createVoucher,
    getAllVoucher: getAllVoucher,
    getVoucherById: getVoucherById,
    updateVoucherById: updateVoucherById,
    deleteVoucherById: deleteVoucherById
};