
//Import prizeModel vào
const prizeModel = require('../models/prizeModel');

//Khai báo thư viện mongoose để tạo _id
var mongoose = require('mongoose');

//Hàm tạo mới user
const createPrize = (request, response) => {
  // B1: Thu thập dữ liệu
  let bodyRequest = request.body;
  // B2: Kiểm tra dữ liệu
  if (!bodyRequest.name) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "name is required"
    })
  }
  // B3: Thao tác với cơ sở dữ liệu
  let createPrize = {
    _id: new mongoose.Types.ObjectId(),
    name: bodyRequest.name,
    description: bodyRequest.description
  }
  prizeModel.create(createPrize, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Create prize success",
        data: data
      })
    }

  })
}
//Hàm lấy hết danh sách prize
const getAllPrize = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  //B2: Validate dữ liệu
  //B3: Thao tác với cơ sở dữ liệu
  prizeModel.find((error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Get all prizes success",
        data: data
      })
    }
  })
}
//Hàm lấy ra 1 voucher theo ID
const getPrizeById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let prizeId = request.params.prizeId;
  console.log(prizeId);
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(prizeId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "Prize ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  prizeModel.findById(prizeId, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Get prize by id success",
        data: data
      })
    }
  })
}
//Hàm cập nhật 1 prize theo ID
const updatePrizeById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let prizeId = request.params.prizeId;
  let bodyRequest = request.body;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(prizeId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "Prize ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  let updatePrize = {
    name: bodyRequest.name,
    description: bodyRequest.description
  }
  prizeModel.findByIdAndUpdate(prizeId, updatePrize, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Update prize by id success",
        data: data
      })
    }
  })
}

//Hàm xoá 1 prize theo ID
const deletePrizeById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let prizeId = request.params.prizeId;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(prizeId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "Prize ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  prizeModel.findByIdAndDelete(prizeId, (error) => {
    if(error) {
        return response.status(500).json({
            status: "Error 500: Internal server error",
            message: error.message
        })
    } else {
        response.status(204).json({
          status: "Success: Delete prize success"
        })
    }
})
}
module.exports = {
    createPrize: createPrize,
    getAllPrize: getAllPrize,
    getPrizeById: getPrizeById,
    updatePrizeById: updatePrizeById,
    deletePrizeById: deletePrizeById
};