//Import diceHistoryModel vào
const diceHistoryModel = require('../models/diceHistoryModel');

//Import userModel vào
//const userModel = require('../models/userModel');

//Khai báo thư viện mongoose để tạo _id
var mongoose = require('mongoose');

//Hàm tạo mới 1 orders
const createDiceHistory = (request, response) => {
  // B1: Thu thập dữ liệu
  //let userId = request.params.userId;
  let bodyRequest = request.body;
  // B2: Kiểm tra dữ liệu
  if (!bodyRequest.user) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "user is required"
    })
  }
  if (!bodyRequest.dice) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "dice is required"
    })
  }
  // B3: Thao tác với cơ sở dữ liệu
  let createDiceHistory = {
    _id: new mongoose.Types.ObjectId(),
    user:mongoose.Types.ObjectId(),
    dice: bodyRequest.dice
  }
  diceHistoryModel.create(createDiceHistory, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      
      response.status(200).json({
        status: "Success: Update history by id success",
        data: data
      })
    }

  })
}

//Hàm lấy hết dice history
const getAllDiceHistory = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  //B2: Validate dữ liệu
  //B3: Thao tác với cơ sở dữ liệu
  diceHistoryModel.find((error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Get all dice history success",
        data: data
      })
    }
  })
}

//Hàm lấy 1 dice history theo ID
const getDiceHistoryById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let historyId = request.params.diceHistoryId;
  console.log(historyId);
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(historyId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "History ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  diceHistoryModel.findById(historyId, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Get history by id success",
        data: data
      })
    }
  })
}

//Hàm update dice history by ID
const updateDiceHistoryById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let historyId = request.params.diceHistoryId;
  let bodyRequest = request.body;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(historyId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "Dice history ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  let updateHistory = {
    user:mongoose.Types.ObjectId(),
    dice: bodyRequest.dice
  }
  diceHistoryModel.findByIdAndUpdate(historyId, updateHistory, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Update history by id success",
        data: data
      })
    }
  })
}

//Hàm xoá 1 dice history by ID
const deleteDiceHistoryById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let historyId = request.params.diceHistoryId;
  //let userId = request.params.userId;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(historyId)) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "History ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  diceHistoryModel.findByIdAndDelete(historyId, (error) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      //sau khi xoá xong 1 order thì cần phải xoá orderID đó ra khỏi user tương ứng        
      response.status(204).json({
        status: "Success: Delete dice history success"
      })
    }
  })

}
module.exports = {
  createDiceHistory: createDiceHistory,
  getAllDiceHistory: getAllDiceHistory,
  getDiceHistoryById: getDiceHistoryById,
  updateDiceHistoryById: updateDiceHistoryById,
  deleteDiceHistoryById: deleteDiceHistoryById
};