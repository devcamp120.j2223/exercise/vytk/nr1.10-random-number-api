//Import prizeHistoryModel vào
const prizeHistoryModel = require('../models/prizeHistoryModel');

//Khai báo thư viện mongoose để tạo _id
var mongoose = require('mongoose');

//Hàm tạo mới 1 orders
const createPrizeHistory = (request, response) => {
  // B1: Thu thập dữ liệu
  let bodyRequest = request.body;
  // B2: Kiểm tra dữ liệu
  if (!bodyRequest.user) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "user is required"
    })
  }
  if (!bodyRequest.prize) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "prize is required"
    })
  }
  // B3: Thao tác với cơ sở dữ liệu
  let createPrizeHistory = {
    _id: new mongoose.Types.ObjectId(),
    user:mongoose.Types.ObjectId(),
    prize: bodyRequest.prize
  }
  prizeHistoryModel.create(createPrizeHistory, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      
      response.status(200).json({
        status: "Success: Update history by id success",
        data: data
      })
    }

  })
}

//Hàm lấy hết prize history
const getAllPrizeHistory = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  //B2: Validate dữ liệu
  //B3: Thao tác với cơ sở dữ liệu
  prizeHistoryModel.find((error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Get all prize history success",
        data: data
      })
    }
  })
}

//Hàm lấy 1 prize history theo ID
const getPrizeHistoryById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let historyId = request.params.historyId;
  console.log(historyId);
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(historyId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "History ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  prizeHistoryModel.findById(historyId, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Get prize history by id success",
        data: data
      })
    }
  })
}

//Hàm update dice history by ID
const updatePrizeHistoryById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let historyId = request.params.historyId;
  let bodyRequest = request.body;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(historyId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "Prize history ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  let updateHistory = {
    user:mongoose.Types.ObjectId(),
    prize: bodyRequest.prize
  }
  prizeHistoryModel.findByIdAndUpdate(historyId, updateHistory, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Update prize history by id success",
        data: data
      })
    }
  })
}

//Hàm xoá 1 prize history by ID
const deletePrizeHistoryById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let historyId = request.params.historyId;
  //let userId = request.params.userId;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(historyId)) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "History ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  prizeHistoryModel.findByIdAndDelete(historyId, (error) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      //sau khi xoá xong 1 order thì cần phải xoá orderID đó ra khỏi user tương ứng        
      response.status(204).json({
        status: "Success: Delete prize history success"
      })
    }
  })

}
module.exports = {
    createPrizeHistory: createPrizeHistory,
    getAllPrizeHistory: getAllPrizeHistory,
    getPrizeHistoryById: getPrizeHistoryById,
    updatePrizeHistoryById: updatePrizeHistoryById,
    deletePrizeHistoryById: deletePrizeHistoryById
};