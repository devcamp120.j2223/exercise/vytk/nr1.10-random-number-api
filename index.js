// Lệnh này tương tự import express from 'express' // Dùng để import thư viện express vào project
const express = require('express');
const path = require('path');
// const diceModel=require('./app/models/diceHistoryModel');
// const userModel=require('./app/models/userModel');
const usersRouter=require('./app/routes/userRouter');
const historyDiceRouter=require('./app/routes/diceHistoryRouter');
const historyPrizeRouter=require('./app/routes/prizeHistoryRouter');
const voucherHistoryRouter=require('./app/routes/voucherHistoryRouter');
const voucherRouter=require('./app/routes/voucherRouter');
const prizeRouter=require('./app/routes/prizeRouter');
//Import mongoosejs
const mongoose=require('mongoose');

// Khởi tạo app express
const app=express();

app.use(express.json());

app.use(express.urlencoded({
    extended:true
}))
// Khai báo cổng project
const port=8000;

mongoose.connect("mongodb://localhost:27017/CRUD_LuckyDice",(err)=>{
    if(err){
        throw err;
    }
    console.log("Connect mongoDB successfully!");
})

app.use((request,response,next)=>{
    console.log("Time ", new Date());
    next();
},
(request,response,next)=>{
    console.log("Request Method ", request.method);
    next();
}
);
app.use('/', usersRouter);
app.use('/', historyDiceRouter);
app.use('/', historyPrizeRouter);
app.use('/', voucherHistoryRouter);
app.use('/',voucherRouter);
app.use('/',prizeRouter);
// Khai báo API dạng get '/'
//Call back function: Là một tham số của hàm khác và nó sẽ được thực thi sau khi hàm đó được call
app.use(express.static(`views/task 23B60`)); // Use this for show image

app.get("/", (request, response) => {
  console.log(`__dirname: ${__dirname}`);
  response.sendFile(path.join(`${__dirname}/views/task 23B60/Task23B40.html`));
})


//Chạy app express
app.listen(port,()=>{
    console.log(`App listening on port: ${port}`);
})
